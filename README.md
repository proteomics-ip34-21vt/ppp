# PPP

[![pipeline status](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/ppp/badges/master/pipeline.svg)](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/ppp/-/commits/master)
[![coverage report](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/ppp/badges/master/coverage.svg)](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/ppp/-/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)

Philosopher hard codes the working directory of the workspace on initialization
which poses problems for relocating it (for example for running on different
computing nodes).

The information is saved in the meta.bin file and encoded in the MessagePack
format. As that is well specified, it is merely loaded, the relevant paths
updated, and then saved back.

# Installation

```
poetry build
pip install ppp-0.1.0-py3-none-any.whl
```

# Usage

```
usage: ppp [-h] -p PATH meta_file

Fix hardcoded paths in philosopher meta data.

positional arguments:
  meta_file             Path to philosopher meta data file.

options:
  -h, --help            show this help message and exit
  -p PATH, --path PATH  New path for philosopher to use. Needs to be the parent of the new '.meta' directory.
```
