"""Update working directory information for philosopher.

Philosopher hard codes the working directory of the workspace on initialization
which poses problems for relocating it (for example for running on different
computing nodes).

The information is saved in the meta.bin file and encoded in the MessagePack
format. As that is well specified, it is merely loaded, the relevant paths
updated, and then saved back.
"""

import argparse
import os

import msgpack


def fix_paths(meta_file: str, new_path: str) -> None:
    """Rewrite paths in philosopher meta data.

    Rewrites the "Home", "MetaFile", "MetaDir", "Dir", "Temp"(joined with /temp) meta data to use a
    different path.

    This rewrites the provided meta data file!

    Args:
    ----
        meta_file (str): Path to a philosopher meta data file (typically "meta.bin").
        new_path (str): New path that will be written into the meta data file.
    """
    with open(meta_file, "rb") as data_file:
        byte_data = data_file.read()

    data = msgpack.unpackb(byte_data)
    data["Home"] = new_path
    data["MetaFile"] = os.path.join(new_path, ".meta/meta.bin")
    data["MetaDir"] = os.path.join(new_path, ".meta")
    data["DB"] = os.path.join(new_path, ".meta/db.bin")
    data["ProjectName"] = os.path.basename(new_path)
    data["Dir"] = new_path
    data["Temp"] = os.path.join(new_path, "temp")

    with open(meta_file, "wb") as outfile:
        packed = msgpack.packb(data)
        outfile.write(packed)


def run() -> None:
    """Run the CLI parser and conversion."""
    parser = argparse.ArgumentParser(
        description="Fix hardcoded paths in philosopher meta data."
    )
    parser.add_argument("meta_file", help="Path to philosopher meta data file.")
    parser.add_argument(
        "-p",
        "--path",
        help="New path for philosopher to use. Needs to be the parent of the new '.meta' directory.",
        required=True,
    )

    args = parser.parse_args()
    fix_paths(args.meta_file, args.path)


if __name__ == "__main__":
    run()
